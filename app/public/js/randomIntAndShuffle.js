'use strict';

module.exports = {
  random,
  shuffle
};


function random(lo,hi) {

  return Math.floor(Math.random() * (hi - lo) + lo);

}
function shuffle(names) {
  let length = names.Length;
  for(let i=0;i<length;i++) {
    let random_index = random(0,length);

    let temp=names[random_index];
    names[random_index] = names[i];
    names[i]=temp;
  }
}
