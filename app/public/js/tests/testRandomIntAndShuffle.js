'use strict';

module.exports = testRandomInt;

const randomInt = require('./../randomIntAndShuffle.js');
const assert = require('assert');

function testRandomInt() {
  test_randomInt_with_lower_bound_included();
  test_randomInt_with_upper_bound_excluded();
  test_randomInt_with_every_number_in_range_included();
  test_shuffle();
}

function test_randomInt_with_lower_bound_included() {
  let i;
  for(i = 0; i < 1000; i++) {
    if(randomInt.random(1,5) === 1) {
      break;
    }
  }
  assert.ok(i < 1000);
}

function test_randomInt_with_upper_bound_excluded() {
  for(let i = 0; i < 1000; i++) {
    if(randomInt.random(1,5) === 5) {
       assert.ok(false);
    }
  }
}

function test_randomInt_with_every_number_in_range_included() {
  let a = [];
  for(let i = 0; i < 1000; i++) {
    a[randomInt.random(1,5)] = 1;
  }
  for(let i = 1; i < 5; i++) {
      assert.ok(a[i] === 1);
  }
}

function test_shuffle() {
  let names = ["thamizh","supriya","karthika","rishi"];

  assert.notEqual(names,randomInt.shuffle(names));
}


testRandomInt();

console.log('All tests passed');
