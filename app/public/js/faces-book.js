$(document).ready(function() {

  // ready for Javascript which will run when the html is loaded
  const faces = $('faces');
  //const add = $('<b>').html('Hello');

  const makeFace = function(name) {
    return $("<img>", {
        title:name,
        class:"face",
        src:`img/2018/${name}.jpg`
      });
  };

  const names = [
    "Akshat",
    "Karthika",
    "Sanjana",
    "Mahidher",
    "Akhil",
    "Keerthana",
    "RVishnuPriya",
    "Anushree",
    "Mariah",
    "Aishu",
    "Rishi",
    "Jon",
    "Pavithrann",
    "Sindhu",
    "Supriya",
    "Shruti",
    "Santhosh",
    "Gayatri",
    "Vishnu",
    "Varsha",
    "John",
    "Ameya",
    "Sudeep",
    "Janani",
    "Thamizh",
    "Thiru"];
  names.forEach(function(name) {
    faces.append(makeFace(name));
  });

});
